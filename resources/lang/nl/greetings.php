<?php

return [
    'lang'=>"Kies uw taal",
    'greeting'=>"Welkom bij het API van Quarantine Fit!",
    'endpoints'=>"Bereikbare eindpunten zijn:",
    'english'=>"Engels",
    'dutch'=>"Nederlands"
];
