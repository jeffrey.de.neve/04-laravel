<?php

return [
    'lang'=>"Choose your language",
    'greeting'=>"Welcome to the Quarantine Fit API!",
    'endpoints'=>"Available endpoints are:",
    'english'=>"English",
    'dutch'=>"Dutch"
];
