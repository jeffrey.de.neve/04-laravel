<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('exercise')->group(function() {
    Route::get('/', 'ExerciseController@all');
    Route::get('/{id}', 'ExerciseController@findById');
});

Route::prefix('workout')->group(function() {
    Route::get('/', 'WorkoutController@all');
    Route::get('{id}', 'WorkoutController@findById');
    Route::get('{id}/exercises/{language}', 'WorkoutController@exercisesOfWorkoutId');
});

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::middleware('auth:api')->group(function() {
    Route::prefix('bmi')->group(function() {
        Route::get('/', 'BmiController@all');
        Route::post('/', 'BmiController@add');
    });

    Route::post('save-subscription', 'NotificationController@register');
});
