<?php

namespace App\Http\Controllers;

use App\Bmi;
use App\NewBmiNotification;
use Illuminate\Http\Request;

class BmiController extends Controller
{
    function all() {
        $user_id = auth('api')->user()->id;
        return Bmi::where('user_id', '=', $user_id)->get();
    }

    function add(Request $request) {
        $bmi = new Bmi;

        $height = $request->height;
        $weight = $request->weight;
        $bmi_value = $weight/(($height/100)*($height/100));

        $user = auth('api')->user();

        $bmi->user_id = $user->id;
        $bmi->height = $height;
        $bmi->weight = $weight;
        $bmi->bmi =round($bmi_value, 1);

        $bmi->save();
        //$user->notify(new NewBmiNotification());

        return $bmi;
    }
}
