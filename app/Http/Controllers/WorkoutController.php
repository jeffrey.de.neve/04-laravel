<?php

namespace App\Http\Controllers;

use App\Workout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class WorkoutController extends Controller
{
    function all() {
        return Workout::all();
    }

    function findById($id) {
        return Workout::find($id);
    }

    function exercisesOfWorkoutId($id, $language) {
        return DB::table('exercise_workout')
            ->join("exercises", "exercises.id", "=", "exercise_workout.exercise_id")
            ->join("exercises_language", "exercises_language.exercise_id", "=", "exercises.id")
            ->where("exercise_workout.workout_id", $id)
            ->where("exercises_language.language", $language)
            ->orderBy("exercise_workout.id")
            ->get(["exercise_workout.*", "exercises_language.name", "exercises_language.description", "exercises.image"]);
    }
}
