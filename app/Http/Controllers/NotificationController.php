<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    function register(Request $request) {
        $validator = Validator::make($request -> all(), [
            'endpoint'=>'required',
            'keys.auth'=>'required',
            'keys.p256dh'=>'required'
        ]);

        if ($validator->fails()) {
            return response(['errors'=>$validator->errors()]);
        }

        $endpoint = $request->endpoint;
        $token = $request->keys['auth'];
        $key = $request->keys['p256dh'];
        $user = auth('api')->user();
        $user->updatePushSubscription($endpoint, $key, $token);

        return response(['success'=>true]);
    }
}
