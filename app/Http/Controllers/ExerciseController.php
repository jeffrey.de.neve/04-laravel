<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\ExerciseTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ExerciseController extends Controller
{
    function all() {
        return ExerciseTranslation::where('language', App::getLocale())->get();
    }

    function findById($id) {
        return ExerciseTranslation::where([
            ['exercise_id', $id],
            ['language', App::getLocale()]
        ])->first();
    }
}
