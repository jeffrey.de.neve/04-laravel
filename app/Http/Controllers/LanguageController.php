<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    function index($lang = "en") {
        App::setLocale($lang);
        return view('welcome');
    }
}
