<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    public $timestamps = false;

    public function exercises() {
        return $this->belongsToMany(Exercise::class)->withPivot('repetitions');
    }
}
