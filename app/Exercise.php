<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    public $timestamps = false;

    public function translations() {
        return $this->hasMany(ExerciseTranslation::class, "exercise_id");
    }
}
