<?php

namespace App;

use Minishlink\WebPush\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class NewBmiNotification extends Notification
{
    /*
    private $newBmi;

    public function __construct($newBmi) {
        $this->newBmi = $newBmi;
    }
    */

    public function __construct() {
        //
    }

    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('Calculated!')
            ->icon('/assets/images/bmi.png')
            ->body('The result has been calculated. Your new BMI is looking great!')
            ->options(['TTL' => 1000]);
    }
}
