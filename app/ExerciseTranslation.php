<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseTranslation extends Model
{
    protected $table = "exercises_language";
    public $timestamps = false;
}
