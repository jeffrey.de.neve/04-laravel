<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->id();
            $table->string("image");
        });
        Schema::create('exercises_language', function (Blueprint $table) {
           $table->id();
           $table->unsignedBigInteger("exercise_id");
           $table->string("language");
           $table->string("name");
           $table->string("description");

           $table->foreign("exercise_id")->references("id")->on("exercises")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
