<?php

use Illuminate\Database\Seeder;

class WorkoutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /*
     * Exercise ids
     * 1 = Rest
     * 2 = Jumping jacks
     * 3 = Push ups
     * 4 = Squats
     * 5 = Sit-ups
     * 6 = Lunges
     * 7 = Pull-up
     */

    public function run()
    {
        $workouts = [
            ['name' => 'Start to get big', 'image' => 'workout.jpeg'],
            ['name' => 'Just sleeping', 'image' => 'sleep.jpeg'],
            ['name' => 'No rest', 'image' => 'no_rest.jpeg'],
            ['name' => 'Arm killer', 'image' => 'arms.jpeg']
        ];

        foreach($workouts as $workout) {
            App\Workout::create($workout);
        }

        $first_workout = App\Workout::find(1);
        $second_workout = App\Workout::find(2);
        $third_workout = App\Workout::find(3);
        $fourth_workout = App\Workout::find(4);

        $first_workout->exercises()->attach(2, ['repetitions' => 10]);
        $first_workout->exercises()->attach(3, ['repetitions' => 5]);
        $first_workout->exercises()->attach(1, ['repetitions' => 30]);
        $first_workout->exercises()->attach(4, ['repetitions' => 10]);
        $first_workout->exercises()->attach(1, ['repetitions' => 30]);
        $first_workout->exercises()->attach(5, ['repetitions' => 5]);
        $first_workout->exercises()->attach(1, ['repetitions' => 30]);
        $first_workout->exercises()->attach(6, ['repetitions' => 10]);
        $first_workout->exercises()->attach(3, ['repetitions' => 5]);
        $first_workout->save();

        $second_workout->exercises()->attach(1, ['repetitions' => 60]);
        $second_workout->exercises()->attach(1, ['repetitions' => 60]);
        $second_workout->exercises()->attach(1, ['repetitions' => 60]);
        $second_workout->exercises()->attach(1, ['repetitions' => 60]);
        $second_workout->exercises()->attach(1, ['repetitions' => 60]);
        $second_workout->save();

        $third_workout->exercises()->attach(2, ['repetitions' => 30]);
        $third_workout->exercises()->attach(4, ['repetitions' => 15]);
        $third_workout->exercises()->attach(5, ['repetitions' => 15]);
        $third_workout->exercises()->attach(2, ['repetitions' => 30]);
        $third_workout->exercises()->attach(4, ['repetitions' => 15]);
        $third_workout->exercises()->attach(5, ['repetitions' => 15]);
        $third_workout->exercises()->attach(2, ['repetitions' => 30]);
        $third_workout->exercises()->attach(4, ['repetitions' => 15]);
        $third_workout->exercises()->attach(5, ['repetitions' => 15]);

        $fourth_workout->exercises()->attach(3, ['repetitions' => 5]);
        $fourth_workout->exercises()->attach(7, ['repetitions' => 5]);
        $fourth_workout->exercises()->attach(1, ['repetitions' => 5]);
        $fourth_workout->exercises()->attach(3, ['repetitions' => 10]);
        $fourth_workout->exercises()->attach(7, ['repetitions' => 10]);
        $fourth_workout->exercises()->attach(1, ['repetitions' => 10]);
        $fourth_workout->exercises()->attach(3, ['repetitions' => 15]);
        $fourth_workout->exercises()->attach(7, ['repetitions' => 15]);
        $fourth_workout->exercises()->attach(1, ['repetitions' => 15]);
    }
    /*
     * Exercise ids
     * 1 = Rest
     * 2 = Jumping jacks
     * 3 = Push ups
     * 4 = Squats
     * 5 = Sit-ups
     * 6 = Lunges
     * 7 = Pull-up
     */
}
