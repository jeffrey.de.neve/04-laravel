<?php

use Illuminate\Database\Seeder;

class ExerciseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exercises = [
            [
                [
                    'language' => "en",
                    'name' => "Rest",
                    'description' => "Slowly walk around while focusing on your breathing."
                ],
                [
                    'language' => "nl",
                    'name' => "Rust",
                    'description' => "Loop rustig rond terwijl je focust op uw ademhaling."
                ]
            ],
            [
                [
                    'exercise_id' => 2,
                    'language' => "en",
                    'name' => "Jumping jacks",
                    'description' => "Jump while swinging your arms and legs."
                ],
                [
                    'exercise_id' => 2,
                    'language' => "nl",
                    'name' => "Jumping jacks",
                    'description' => "Spring terwijl je je armen en benen zwaait."
                ]
            ],
            [
                [
                    'exercise_id' => 3,
                    'language' => "en",
                    'name' => "Push ups",
                    'description' => "Lay down on your tummy and push yourself up using your arms."
                ],
                [
                    'exercise_id' => 3,
                    'language' => "nl",
                    'name' => "Opdrukken",
                    'description' => "Ga op je buik liggen en duw jezelf omhoog met je armen."
                ]
            ],
            [
                [
                    'exercise_id' => 4,
                    'language' => "en",
                    'name' => "Squats",
                    'description' => "Spread your legs and bend your knees as if like your were going to sit."
                ],
                [
                    'exercise_id' => 4,
                    'language' => "nl",
                    'name' => "Hurken",
                    'description' => "Spreid je benen en buig je knieën alsof je gaat zitten."
                ]
            ],
            [
                [
                    'exercise_id' => 5,
                    'language' => "en",
                    'name' => "Sit-ups",
                    'description' => "Lay on your back and lift the torso into a sitting position."
                ],
                [
                    'exercise_id' => 5,
                    'language' => "nl",
                    'name' => "Sit-ups",
                    'description' => "Ga op je rug liggen en til de romp in een zittende positie."
                ]
            ],
            [
                [
                    'exercise_id' => 6,
                    'language' => "en",
                    'name' => "Lunges",
                    'description' => "Take a step forward and lower your body, then go back up."
                ],
                [
                    'exercise_id' => 6,
                    'language' => "nl",
                    'name' => "Lunges",
                    'description' => "Zet een stap naar voren, laat je lichaam zakken en ga dan weer naar boven."
                ]
            ],
            [
                [
                    'exercise_id' => 7,
                    'language' => "en",
                    'name' => "Pull-up",
                    'description' => "Pull yourself up to a bar using your arms."
                ],
                [
                    'exercise_id' => 7,
                    'language' => "nl",
                    'name' => "Optrekken",
                    'description' => "Trek jezelf met je armen omhoog naar een stok."
                ]
            ]
        ];

        $exercise_images = [
            ['image'=>'rest.jpg'],
            ['image'=>'jumping_jack.jpg'],
            ['image'=>'push_up.jpg'],
            ['image'=>'squat.jpg'],
            ['image'=>'sit_up.jpg'],
            ['image'=>'lunge.jpg'],
            ['image'=>'pull_up.jpg']
        ];

        foreach($exercises as $key => $exercise) {
            $saved_exercise = App\Exercise::create($exercise_images[$key]);
            foreach($exercise as $translation) {
                $translation['exercise_id'] = $saved_exercise->id;
                $translation_model = App\ExerciseTranslation::create($translation);
            }
        }
    }
}
