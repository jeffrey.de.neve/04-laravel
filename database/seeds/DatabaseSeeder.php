<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    /*
     * php artisan migrate:fresh
     * php artisan db:seed
     */

    public function run()
    {
        $this->call(ExerciseTableSeeder::class);
        $this->call(WorkoutTableSeeder::class);
    }
}
